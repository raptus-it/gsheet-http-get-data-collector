# README #

Transmit data automatically into Google Sheets via HTTP GET calls. Includes some data collectors.

### Setup Google Sheet ###

1. Table must be created in personal space (no shared drive)
2. Assign name for the table
3. Delete the unnecessary lines (e.g. from line 5 on)
4. Define the default column titles
	* Line 1, column A is the date
	* Line 1, column B is the time
5. Define the individual column titles and thus the HTTP parameter names

### Setup Scripteditor ###
* Open the Script Editor in the Tools menu
* Set project name (top left, same as file name)
* Enable Google Sheets API
	* In menu Resources
	* Select advanced Google services
	* Enable Google Sheets API
* Insert the [code for the WebApp](https://bitbucket.org/gzuercher/gsheet-http-get-data-collector/src/master/GAS-WebApp-Server/Code.gs) 
* Save the project

### Publish WebApp ###
Upon each change in the code the production WebApp needs to be published again. This does not apply to the development version. To publish:

* In the Publish menu, select "Set up as WebApp
* Make the following settings
	* * Project version new
	* * Execute Me
	* * Access Anyone, even anonymous (depends on your collector)
* Grant access
* Copy/save URL and use for data collection

### Operating principle ###

1. The query string must be added to the URL previously generated in the table
2. The HTTP GET command is executed (see [code examples](https://bitbucket.org/gzuercher/gsheet-http-get-data-collector/src/master/Example-Clients/))
3. Each time the URL is called, the values are added as a new row in the table

### Generating the Query String ###
* Insert a question mark after the last part of the URL (http.../exec) to start with the parameters
* A parameter has a name and a value, the two are separated by the = character
* Each parameter must be separated from the next with the & character
* Certain characters are not supported in URLs (e.g. spaces), encoding is necessary

### Assignment of data to columns ###
To assign parameter names from the URL, the column titles in the table are used. The names are looked up in the table and searched in the query string. So the "boss" is the table. 

If blanks in the column titles are to be used, they would also have to be URL encoded. It is recommended not to do this and to use only upper and lower case letters without other characters for the parameter names.

### Data in multiple tabs ###
With the predefined parameter "sheet" the tab within the table can be selected. For this purpose, the name of the tab must be specified as value. If the parameter is not found, the data is automatically added to the first tab.

### Missing allocations ###
If the query string contains parameters for which no column exists in the table, the data is ignored and no error message is displayed. A wrong spelling can also be the cause.

If parameters expected by the table are not found in the query string, the value is entered in the variable "novalue" (at the very top of the script) in the table. This can be adjusted.

### Number of columns ###
With the variable "maxcols" (at the top of the script) the number of columns and query string parameters can be changed. The smaller this number is, the faster the execution of the script will be. This can be interesting in some cases with little data and a high frequency.
