function doGet(e) {
  var maxcols = 150;
  var novalue = "-";
  var tz = Session.getScriptTimeZone();
  var dt = new Date();
  
  var newrow = [];
  newrow.push(Utilities.formatDate(dt, tz, "dd.MM.yyyy"));
  newrow.push(Utilities.formatDate(dt, tz, "HH:mm:ss"));
 
  var sheet;
  if(e.parameter["sheet"])
    sheet = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(e.parameter["sheet"]);
  else
    sheet = SpreadsheetApp.getActiveSpreadsheet().getSheets()[0];
  
  if(!sheet)
    return ContentService.createTextOutput("Sheet Tab " + sheet + " not found");
  
  var fline = sheet.getRange(1, 1, 1, maxcols).getValues();
  for (col = 0; col <= maxcols; col++) {
    if(col > 1 && fline[0][col]) {
      if(e.parameter[fline[0][col]])
        newrow.push( decodeURI( e.parameter[fline[0][col]] ) );
      else
        newrow.push(novalue);
    }
  }

  sheet.appendRow(newrow);
  return ContentService.createTextOutput('OK');
}
