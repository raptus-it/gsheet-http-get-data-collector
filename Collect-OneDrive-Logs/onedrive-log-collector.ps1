#
# Collect data from SyncDiagnostics.log and transmit it to GSheet
# Based on https://call4cloud.nl/2020/09/lost-in-monitoring-onedrive/
#

# URI of Google Sheets target
$gsuri = "https://script.google.com/macros/s/.../exec"

# Maximum time (negative value in minutes) the SyncDiagnostics.log files
# have been changed in order to be processed by this script
$logfilemaxage = -1440

# Which items in SyncDiagnostics.log have to be processed / transmitted
# Must be the same order as in target Google Sheet table
$logvars = @(
"BytesDownloaded",
"BytesToDownload",
"BytesToUpload",
"BytesUploaded",
"ChangesToProcess",
"ChangesToSend",
"DownloadSpeedBytesPerSec",
"EstTimeRemainingInSec",
"FilesToDownload",
"FilesToUpload",
"OfficeSyncActive",
"OfficeSyncEnabled",
"SymLinkCount",
"UploadSpeedBytesPerSec",
"activeHydrations",
"appId",
"bytesAvailableOnDiskDrive",
"clientType",
"clientVersion",
"conflictsFailed",
"conflictsHandled",
"cpuPercentage",
"datVersion",
"dehydrations",
"driveChangesToSend",
"driveSentChanges",
"drivesChangeEnumPending",
"drivesConnected",
"drivesScanRequested",
"drivesWaitingForInitialSync",
"failedBytesDownloadedTotal",
"failedBytesUploadedTotal",
"files",
"filesToDownload",
"flavor",
"folders",
"fullScanCount",
"invalidatedScanCount",
"numAbortedMissingServerChanges",
"numAbortedNoLongerIdle",
"numAbortedReSyncNeeded",
"numAbortedUnexpectedHttpStatus",
"numAbortedWatcherEtagDifference",
"numDeleteConvToUnmap",
"numDownloadErrorsReported",
"numDrives",
"numDrivesNeedingEventualScan",
"numExternalFileUploads",
"numFileDownloads",
"numFileFailedDownloads",
"numFileFailedUploads",
"numFileInWarning",
"numFileUploads",
"numHashMismatchErrorsReported",
"numLcChangeFile",
"numLcChangeFolder",
"numLcCreateFile",
"numLcCreateFolder",
"numLcDeleteFile",
"numLcDeleteFolder",
"numLcMoveFile",
"numLcMoveFolder",
"numLocalChanges",
"numRealizerErrorsReported",
"numResyncs",
"numSelSyncDrives",
"numUploadErrorsReported",
"officeVersion",
"officeVersionDot",
"passiveHydrations",
"placeholdersEnabled",
"preciseScanCount",
"scanState",
"scanStateStallDetected",
"seOfficeFiles",
"seOfficeFilesToDownload",
"seOfficeFilesToUpload",
"successfulBytesDownloadedTotal",
"successfulBytesUploadedTotal",
"syncProgressState",
"syncStallDetected",
"threadCount",
"totalDuration",
"totalSubScopes",
"uptimeSecs",
"userOverriddenConcurrentUploads",
"vaultState",
"version",
"wasFileDBReset",
"StallErrorSummary"
)

# Retrieve all relevant logfiles
$folderMask = "C:\Users\*\AppData\Local\Microsoft\OneDrive\logs\Business1\*.*"
$logfiles = Get-ChildItem -Path $folderMask -Filter SyncDiagnostics.log |  Where-Object { $_.LastWriteTime -gt [datetime]::Now.AddMinutes($logfilemaxage)}

# Retrieve global info
$ipinfo = (Invoke-RestMethod -uri "http://ipinfo.io")
$sysinfo = Get-WmiObject -Class Win32_ComputerSystem

# Parse each logfile individually
foreach($logfile in $logfiles) {
    $username = $logfile.DirectoryName.split("\") | Select-Object -Index 2
    Write-Host -NoNewline "Processing: $username `t`t`t"
    $procinfo = Get-Process OneDrive -IncludeUserName -ErrorAction SilentlyContinue | Where-Object {$_.username -match $username}

    $logcontents = Get-Content $logfile
    $diagreport = $logcontents | Where-Object { $_.Contains("Diagnostic Report") }
    if(!$diagreport) {
        Write-Host "Skipping due to incomplete logfile"
        continue;
    }

    $gsparams = new-object System.Collections.Hashtable
    $gsparams.Add("Username",           [uri]::EscapeDataString($username))
    $gsparams.Add("Hostname",           [uri]::EscapeDataString([System.Net.Dns]::GetHostByName($env:computerName)[0].HostName))
    $gsparams.Add("IPPub",              [uri]::EscapeDataString($ipinfo.ip))
    $gsparams.Add("IPOrg",              [uri]::EscapeDataString($ipinfo.org))
    $gsparams.Add("OneDriveCPU",        [uri]::EscapeDataString($(if($procinfo) { $procinfo[0].CPU } else { "-" } )))
    $gsparams.Add("SyncProgressState",  [uri]::EscapeDataString($($logcontents | Where-Object { $_.Contains("SyncProgressState") }).Split(":", 2)[1]))
    $gsparams.Add("UtcNow",             [uri]::EscapeDataString($($logcontents | Where-Object { $_.Contains("UtcNow") }).Split(":", 2)[1]))

    foreach($logvar in $logvars) {
        $logvalue = $logcontents | Where-Object { $_.Contains($logvar) }
        if($logvalue) { $logvalue = $logvalue.Split("=", 2) | Select -Index 1 } else { $logvalue = "-" }
        $gsparams.Add($logvar, [uri]::EscapeDataString($logvalue.Trim()))
    }

    $status = Invoke-WebRequest -Method GET -uri $gsuri -Body $gsparams | Select-Object -Expand StatusCode
    if($status = 200) {
        Write-Host "OK"
    }
    else {
        Write-Host "Failed"
    }
}

